FROM python:3.6-slim-stretch

RUN apt-get update -qq && \
        apt-get -y --no-install-recommends install \
        binutils \
        curl \
        gcc \
        git \
        make \
        && rm -rf /var/lib/apt/lists/*

RUN pip install git+https://gitlab.com/rveach/tdlutils.git
RUN pip install pyinstaller

WORKDIR /usr/local/bin

CMD pyinstaller --onefile transmission_post_download.py

