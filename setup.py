from distutils.core import setup
setup(
  name = 'tdlutils',
  packages = ['tdlutils'],
  version = '0.1.4',
  description = 'Help Manage Transmission',
  author = 'ryan',
  author_email = 'ryan@example.com',
  url = 'https://gitlab.com/rveach/tdlutils',
  download_url = 'https://gitlab.com/rveach/tdlutils/-/archive/master/tdlutils-master.tar.gz',
  keywords = ['transmission'], # arbitrary keywords
  classifiers = [],
  install_requires=[],
  scripts=['scripts/transmission_post_download.py'],
)
