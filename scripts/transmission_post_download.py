#! /usr/bin/env python
"""
	This script is meant to be run following a transmission download.
"""

import argparse
import configparser
import json
import os
import shutil

import tdlutils

def write_torrent_info(torrent_hash, config):
	"""
		accepts torrent hash string, anticipating the environment variables that
		are usually passed along from the transmission post download script.
		https://trac.transmissionbt.com/wiki/Scripts#OnTorrentCompletion
	"""
	td = tdlutils.transmission(
		config['TRANSMISSION']['USERNAME'],
		config['TRANSMISSION']['PASSWORD'],
		host=config['TRANSMISSION']['HOST'],
		#port=config['TRANSMISSION']['PORT']
	)
	torrent = tdlutils.torrent(td, torrent_hash)
	torrent_info = torrent.getfullinfo()

	# create a temporary file while we're writing.
	json_file_path = os.path.join(
		config['POSTDOWNLOAD']['JSON_LOCATION'],
		"%s.tmp" % torrent_info['transmission_hash']
	)

	f = open(json_file_path, "w")
	json.dump(torrent_info, f)
	f.close()

	# move from the temp location to the .new file.
	shutil.move(
		json_file_path,
		os.path.join(
			config['POSTDOWNLOAD']['JSON_LOCATION'],
			"%s.new" % torrent_info['transmission_hash']
		)
	)

if __name__ == '__main__':
	


	# get the one env variable we need, run function
	# https://trac.transmissionbt.com/wiki/Scripts#OnTorrentCompletion
	
	parser = argparse.ArgumentParser()
	parser.add_argument(
		"-t", "--torrent",
		help="Pass the torrent hash or id",
		default=os.getenv("TR_TORRENT_HASH", None)
	)
	parser.add_argument(
		"-c", "--config",
		help="specify config file location",
		default=os.path.join(os.getenv("HOME"), "tdl_config.ini"),
	)
	args = parser.parse_args()

	# set and check config file location
	CONFIG_FILE_LOCATION = args.config
	if not os.path.exists(CONFIG_FILE_LOCATION):
		raise FileNotFoundError("Could not find the config file")

	# read config
	TDL_CONFIG = configparser.ConfigParser()
	TDL_CONFIG.read(CONFIG_FILE_LOCATION)

	if args.torrent:
		write_torrent_info(args.torrent, TDL_CONFIG)
	else:
		raise RuntimeError("torrent id or hash not set.  Use --torrent or set environment variable TR_TORRENT_HASH")
