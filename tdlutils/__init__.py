#! /usr/bin/env python
"""
	This module helps manage Transmisison using mostly transmission remote
	https://linux.die.net/man/1/transmission-remote
"""

import hashlib
import os
import subprocess

def sha256_checksum(filename, block_size=65536):
	sha256 = hashlib.sha256()
	with open(filename, 'rb') as f:
		for block in iter(lambda: f.read(block_size), b''):
			sha256.update(block)
	return sha256.hexdigest()

class torrent:
	"""
		keeps info for a torrent using a transmission client
	"""

	def getfullinfo(self):

		torrent_info = {
			"name": self.name,
			"percent_done": self.percent_done,
			"transmission_id": self.transmission_id,
			"transmission_hash": self.transmission_hash,
			"location": self.location,
			"files": self.files
		}
		return torrent_info

	def ext_file_info(self):
		"""
			gets extended file info
			- full path
			- does it exist
			- hash
			- size
		"""

		all_file_info = []

		for f in self.filebytes:

			# str(bytes_string, 'utf-8')
			fullpath = str(os.path.join(self.infobytes[b'location'], f), 'utf-8')
			if os.path.exists(fullpath):
				this_file = {
					"fullpath": fullpath,
					"exists": True,
					"hash": sha256_checksum(fullpath),
					"size": os.path.getsize(fullpath)
				}
			else:
				this_file = {
					"fullpath": fullpath,
					"exists": False,
					"hash": None,
					"size": None
				}
			all_file_info.append(this_file)
		return all_file_info


	def get_info(self):
		info = self.transmission.torrent_info(self.torrent_id)
		return info

	def get_files(self):
		files = self.transmission.torrent_files(self.torrent_id)
		return files

	def __init__(self, transmission, torrent_id):
		""" initialize """
		self.transmission = transmission
		self.torrent_id = torrent_id

		""" gathered info """
		# general torrent info - returns everything in bytes
		self.infobytes = self.get_info()
		# list of files, returned in bytes
		self.filebytes = self.get_files()

		"""
			Usable information
			- should be in strings
			- should easily report info we need
		"""
		self.files = self.ext_file_info()
		self.location = str(self.infobytes[b'location'], 'utf-8')
		self.transmission_hash = str(self.infobytes[b'hash'], 'utf-8')
		self.transmission_id = int(self.infobytes[b'id'])
		self.percent_done = int(self.infobytes[b'percent_done'].strip(b'%'))
		self.name = str(self.infobytes[b'name'], 'utf-8')



class transmission:
	"""
		class used to communicate with a transmission torrent client.
	"""

	"""
	# this method may be useful in the future, but not right now.
	def listall(self):
		# transmission-remote host:9091 --auth=username:password -l
		
		torrents = []
		
		cmd = [self.transmission_remote, self.hoststring, self.authstring, "-l"]
		p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)

		stdout_lines = p.stdout.split(bytes(os.linesep, 'utf-8'))

		for line in stdout_lines:
			linefields = line.strip().split(None, 9)
			
			if linefields[0].upper() == "ID":
				pass # this is our header

			elif linefields[0].upper() == "SUM:":
				pass # this is our summary
				# Sum:          794.5 GB             122.0     0.0

			# make sure we have enough fields to parse
			elif len(linefields) >= 8:

				# convert gb to mb
				have_mb = float(linefields[2])
				if linefields[3].upper() == "GB":
					have_mb = have_mb * 1024

				this_torrent = {
					"id": linefields[0].strip(b'*'),
					"percent_done": int(linefields[1].strip(b'%')),
					"have_mb": have_mb
				}


		return torrents
	"""

	def torrent_info(self, torrent_id):
		"""
			gathers torrent info from id
		"""
		# (tempvenv) ryan@ks393682:~/tempvenv$ transmission-remote 127.0.0.1:9112 --auth=claudia:IShootYouWithMyLightningGun -t 700 -i
		cmd = [self.transmission_remote, self.hoststring, self.authstring, "-t", str(torrent_id), "-i"]
		p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
		stdout_lines = p.stdout.split(bytes(os.linesep, 'utf-8'))

		torrent_info = {}

		for line in stdout_lines:
			line = line.split(b':', 1)

			if len(line) == 2:
				torrent_info[line[0].strip().lower().replace(b' ', b'_')] = line[1].strip()

		return torrent_info

	def torrent_files(self, torrent_id):
		"""
			gathers a list of all files
		"""
		cmd = [self.transmission_remote, self.hoststring, self.authstring, "-t", str(torrent_id), "-f"]
		p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
		stdout_lines = p.stdout.split(bytes(os.linesep, 'utf-8'))

		files = []

		for line in stdout_lines:
			line = line.strip().split(None, 6)
			if (len(line) >= 5) and (line[0] != b'#'):
				files.append(line[6])

		return files


		

	def __init__(self, user, password, host="127.0.0.1", port=9091, transmission_remote="/usr/bin/transmission-remote"):
		"""
			initialize object
		"""
		self.username = user
		self.password = password
		self.host = host
		#self.port = port
		self.authstring = "--auth=%s:%s" % (self.username, self.password)
		self.hoststring = self.host
		self.transmission_remote = transmission_remote
